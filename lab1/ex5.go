package main

import "fmt"

func main() {
	fmt.Println("Синонимы целых типов\n")

	fmt.Println("byte    - int8")
	fmt.Println("rune    - int32")
	fmt.Println("int     - int32, или int64, в зависимости от ОС")
	fmt.Println("uint    - uint32, или uint64, в зависимости от ОС")

	var test int = 1 << 32
	test++
	fmt.Println("Your system x64")
	//Задание.
	//1. Определить разрядность ОС
}
