package main

import (
	"fmt"
	"math"
)

func main() {
	var masElem [2000]int
	var masElemFloat [2000]float64
	var masP [151]float64
	minValue := 0
	maxValue := 151
	var mathHope float64 = 0
	var dispersion float64 = 0
	var x int = 137

	//Shift + alt + F - format doc

	x = (1664525*x + 1013904223) % int(math.Pow(2, 32))
	masElem[0] = minValue + x%maxValue
	for i := 0; i < len(masElem)-1; i++ {
		x = (1664525*x + 1013904223) % int(math.Pow(2, 32))
		masElem[i+1] = minValue + x%maxValue
	}

	for i := 0; i < len(masP); i++ {
		for j := 0; j < len(masElem); j++ {
			if i == masElem[j] {
				masP[i]++
			}
		}
		masP[i] /= 2000
		fmt.Printf("%d ", i)
		fmt.Println(masP[i])
		mathHope += float64(i) * masP[i]
	}
	fmt.Printf("MathHope = %f\n", mathHope)
	for i := 0; i < len(masP); i++ {
		dispersion += math.Pow(float64(i)-mathHope, 2) * masP[i]
	}
	fmt.Printf("Dispersion = %f\n", dispersion)
	fmt.Printf("Devivation = %f\n\n", math.Sqrt(dispersion))

	x = 137
	x = (1664525*x + 1013904223) % int(math.Pow(2, 32))
	masElemFloat[0] = float64(x) / (math.Pow(2, 32) / float64(minValue+x%maxValue))
	//fmt.Println(masElemFloat[0])
	for i := 0; i < len(masElem)-1; i++ {
		x = (1664525*x + 1013904223) % int(math.Pow(2, 32))
		masElemFloat[i+1] = float64(x) / (math.Pow(2, 32) / float64(minValue+x%maxValue))
		//Decoment this line to see real numbers
		//fmt.Println(masElemFloat[i+1])
	}
}
