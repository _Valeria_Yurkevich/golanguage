package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/sessions"
)

const (
	pageHeader = `<!DOCTYPE HTML> 
	<html> 
	<head> 
		<title>Task 1</title> 
		<style> 
			.error{ color:#FF0000; 
		</style> 
	</head>`
	pageBody = `<body> 
		<h1>Sign in</h1>`
	form = `<form action="/me" method="POST"> 
		<label for="login">Login:</label><br /> 
		<input type="text" name="login" size="20"><br />
		<label for="password">Password:</label><br /> 
		<input type="password" name="password" size="20"><br /> 
		<input type="submit" value="Sign in"> </form>
		<p>Valid login=login, password=password</p>`
	pageFooter = `</body></html>`
	anError    = `<p class="error">%s</p>`
)

var store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

func SignIn(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form)
	fmt.Fprint(w, "\n", pageFooter)
}

func Profile(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		login := post.Get("login")
		password := post.Get("password")
		if login == "login" && password == "password" {
			session, err := store.Get(r, "user")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			session.Values["username"] = login
			session.Save(r, w)
			fmt.Fprintf(w, "Welcome, "+session.Values["username"].(string))
		} else {
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	}
}

func main() {
	http.HandleFunc("/", SignIn)
	http.HandleFunc("/me", Profile)
	http.ListenAndServe("localhost:8081", nil)
}
