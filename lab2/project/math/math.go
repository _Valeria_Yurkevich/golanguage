package math

// Сумма трех чисел.
func Add(x1 float64, x2 float64, x3 float64) float64 {
	total := x1 + x2 + x3
	return total
}

func Min(x1 float64, x2 float64, x3 float64) float64 {
	if x2 >= x1 && x1 <= x3 {
		return x1
	} else if x1 >= x2 && x2 <= x3 {
		return x2
	} else {
		return x3
	}	
}

func Average(x1 float64, x2 float64, x3 float64) float64 {
	return (x1 + x2 + x3) / 3	
}

func Equality(k float64, x float64, b float64) float64 {
	return k * x + b	
}


