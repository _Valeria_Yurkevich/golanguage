package math

import "testing"
import m "math"

func TestAdd(t *testing.T) {
	x := m.Add(1, 2, -3)
	res := 0.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestMin(t *testing.T) {
	x := m.Min(1, 2, -3)
	res := -3.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestAverage(t *testing.T) {
	x := m.Average(1, 2, -3)
	res := 0.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestEquality(t *testing.T) {
	x := m.Equality(1, 2, -3)
	res := -1.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}