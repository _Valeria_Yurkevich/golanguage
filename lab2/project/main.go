// main
package main

import "fmt"
import math "./math"

func main() {
	sum := math.Add(1, 2, -3)
	fmt.Println(sum)
	min := math.Min(1.3, 2.33, -3.2)
	fmt.Println(min)
	avg := math.Average(1.3, 2.33, -3.2)
	fmt.Println(avg)
	equ := math.Equality(1.3, 2.33, -3.2)
	fmt.Println(equ)
}
