package main

import (
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func initGUI() {
	window := ui.NewWindow("Hello", 500, 200, false)
	window.SetMargined(true)

	hBox := ui.NewHorizontalBox()
	vBoxSize := ui.NewVerticalBox()
	vBoxType := ui.NewVerticalBox()
	hBoxWidth := ui.NewHorizontalBox()
	hBoxHeight := ui.NewHorizontalBox()
	hBoxMaterial := ui.NewHorizontalBox()
	hBoxTotal := ui.NewHorizontalBox()

	hBox.SetPadded(true)
	vBoxSize.SetPadded(true)
	vBoxType.SetPadded(true)
	hBoxWidth.SetPadded(true)
	hBoxHeight.SetPadded(true)
	hBoxMaterial.SetPadded(true)
	hBoxTotal.SetPadded(true)

	labelSize := ui.NewLabel("Size of the window")

	labelWidth := ui.NewLabel("Width (cm):")
	inputWidth := ui.NewEntry()

	labelHeight := ui.NewLabel("Height (cm):")
	inputHeigth := ui.NewEntry()

	labelMaterial := ui.NewLabel("Material:")
	dropDownMaterial := ui.NewCombobox()
	dropDownMaterial.Append("Wood")
	dropDownMaterial.Append("Metal")
	dropDownMaterial.Append("Plastic")

	labelTotal := ui.NewLabel("Total:")
	outPutTotal := ui.NewLabel("")

	labelGlazing := ui.NewLabel("Type of glasing")

	dropDownType := ui.NewCombobox()
	dropDownType.Append("Simple glasing")
	dropDownType.Append("Double glasing")

	checkBox := ui.NewCheckbox("Windowsill")

	totalButton := ui.NewButton("Calculate")

	hBoxWidth.Append(labelWidth, false)
	hBoxWidth.Append(inputWidth, false)

	hBoxHeight.Append(labelHeight, false)
	hBoxHeight.Append(inputHeigth, false)

	hBoxMaterial.Append(labelMaterial, false)
	hBoxMaterial.Append(dropDownMaterial, false)

	hBoxTotal.Append(labelTotal, false)
	hBoxTotal.Append(outPutTotal, false)

	vBoxSize.Append(labelSize, false)
	vBoxSize.Append(hBoxWidth, false)
	vBoxSize.Append(hBoxHeight, false)
	vBoxSize.Append(hBoxMaterial, false)
	vBoxSize.Append(hBoxTotal, false)

	vBoxType.Append(labelGlazing, false)
	vBoxType.Append(dropDownType, false)
	vBoxType.Append(checkBox, false)
	vBoxType.Append(totalButton, false)

	hBox.Append(vBoxSize, false)
	hBox.Append(vBoxType, false)

	window.SetChild(hBox)

	totalButton.OnClicked(func(*ui.Button) {
		width, _ := strconv.ParseFloat(inputWidth.Text(), 64)
		height, _ := strconv.ParseFloat(inputHeigth.Text(), 64)
		total := width * height
		var coef float64
		if dropDownMaterial.Selected() == 0 {
			coef = 0.25
		}
		if dropDownMaterial.Selected() == 1 {
			coef = 0.05
		}
		if dropDownMaterial.Selected() == 2 {
			coef = 0.15
		}
		if dropDownType.Selected() == 1 {
			coef += 0.05
		}
		total *= coef
		if checkBox.Checked() == true {
			total += 35
		}
		outPutTotal.SetText(strconv.FormatFloat(total, 'f', -1, 64) + " UAH")
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
