package main

import (
	"strconv"

	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func initGUI() {
	window := ui.NewWindow("Hello", 500, 200, false)
	window.SetMargined(true)

	hBox := ui.NewHorizontalBox()
	vBox1 := ui.NewVerticalBox()
	vBox2 := ui.NewVerticalBox()
	hBoxDays := ui.NewHorizontalBox()
	hBoxCountry := ui.NewHorizontalBox()
	hBoxTotal := ui.NewHorizontalBox()

	hBox.SetPadded(true)
	vBox1.SetPadded(true)
	vBox2.SetPadded(true)
	hBoxDays.SetPadded(true)
	hBoxCountry.SetPadded(true)
	hBoxTotal.SetPadded(true)

	labelDays := ui.NewLabel("Number of days:")
	inputDays := ui.NewEntry()

	labelCountry := ui.NewLabel("Country:")
	dropDownCountry := ui.NewCombobox()
	dropDownCountry.Append("Bulgaria")
	dropDownCountry.Append("Germany")
	dropDownCountry.Append("Poland")

	labelTotal := ui.NewLabel("Total:")
	outPutTotal := ui.NewLabel("")

	labelPeriod := ui.NewLabel("Period of time")

	dropDownPeriod := ui.NewCombobox()
	dropDownPeriod.Append("Summer")
	dropDownPeriod.Append("Winter")

	checkBoxGuide := ui.NewCheckbox("Individual guide")
	checkBoxLux := ui.NewCheckbox("Lux")

	totalButton := ui.NewButton("Calculate")

	hBoxDays.Append(labelDays, false)
	hBoxDays.Append(inputDays, false)

	hBoxCountry.Append(labelCountry, false)
	hBoxCountry.Append(dropDownCountry, false)

	hBoxTotal.Append(labelTotal, false)
	hBoxTotal.Append(outPutTotal, false)

	vBox1.Append(hBoxDays, false)
	vBox1.Append(hBoxCountry, false)
	vBox1.Append(hBoxTotal, false)

	vBox2.Append(labelPeriod, false)
	vBox2.Append(dropDownPeriod, false)
	vBox2.Append(checkBoxGuide, false)
	vBox2.Append(checkBoxLux, false)
	vBox2.Append(totalButton, false)

	hBox.Append(vBox1, false)
	hBox.Append(vBox2, false)

	window.SetChild(hBox)

	totalButton.OnClicked(func(*ui.Button) {
		days, _ := strconv.ParseFloat(inputDays.Text(), 64)
		var total float64
		if dropDownCountry.Selected() == 0 && dropDownPeriod.Selected() == 0 {
			total = days * 100
		}
		if dropDownCountry.Selected() == 0 && dropDownPeriod.Selected() == 1 {
			total = days * 150
		}
		if dropDownCountry.Selected() == 1 && dropDownPeriod.Selected() == 0 {
			total = days * 160
		}
		if dropDownCountry.Selected() == 1 && dropDownPeriod.Selected() == 1 {
			total = days * 200
		}
		if dropDownCountry.Selected() == 2 && dropDownPeriod.Selected() == 0 {
			total = days * 120
		}
		if dropDownCountry.Selected() == 1 && dropDownPeriod.Selected() == 1 {
			total = days * 180
		}
		if checkBoxLux.Checked() == true {
			total = total / 100 * 120
		}
		if checkBoxGuide.Checked() == true {
			total += days * 50
		}
		outPutTotal.SetText(strconv.FormatFloat(total, 'f', -1, 64) + " USD")
	})

	window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
